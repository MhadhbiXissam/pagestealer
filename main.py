from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import time 
from bs4 import BeautifulSoup
import geckodriver_autoinstaller
geckodriver_autoinstaller.install()
import os 
os.environ['TMPDIR'] = "."




class StealPage : 
    def __init__(self,url = "https://www.google.com/drive/") : 
        self.url = url 
        self.get_page_intial()
        code_no_css = self.without_style(self.code)
        self.default_css = self.get_page_intial_no_css(code_no_css)
        print( self.default_css , file = open( "default.html",'w'))
        print( self.code ,  file = open("original.html",'w'))
        soupd = BeautifulSoup(self.default_css, 'html.parser')
        soupo = BeautifulSoup(self.code, 'html.parser')

        d_with_class = soupd.find_all(attrs={'style': True})
        o_with_class = soupo.find_all(attrs={'style': True})

        # Remove the class attribute from all elements
        for index , element in enumerate(o_with_class):
            styleo = element.get('style')
            csso = self.parse_css(styleo)
            styled = d_with_class[index].get('style')
            cssd = self.parse_css(styled)
            style = dict()
            for ko in csso : 
                if ko in cssd : 
                    if cssd[ko] == csso[ko] : 
                        pass 
                    else : 
                        style[ko] = csso[ko]
                else : style[ko] = csso[ko]
            element["style"] = ";".join([f"{k}:{v}" for k, v in style.items()])

        print(soupd.prettify(),file = open("result.html","w"))
        self.show_result(soupd.prettify())
    def parse_css(self,style) : 
        css = dict()
        for i in style.split(";") : 
            kv = i.strip()
            if len(kv.split(":")) > 0 : 
                if len(kv.split(":")[0].strip()) > 0 : 
                    found = kv.split(":")[0].strip()
                    if not found.startswith("--") : 
                        try : 
                            value = kv.split(":")[1].strip()
                            css[found] = value
                        except Exception as err : 
                            print(i)
        return css 
    def get_page_intial(self) : 
        options = Options()
        #options.add_argument("--headless")
        options.add_argument("--no-remote")
        options.add_argument("--new-instance")
        driver = webdriver.Firefox(options=options)
        driver.get(self.url)
        time.sleep(5)
        self.code  = driver.execute_script("""
        function computeAndSetCSS(root) {
            var result = "";
            // Get all elements in the document
            rootElement = document.querySelector(root);
            var computedStyle = window.getComputedStyle(rootElement);

            // Create a string representation of the computed styles
            var styleString = '';
            for (var i = 0; i < computedStyle.length; i++) {
                var propertyName = computedStyle[i];
                var propertyValue = computedStyle.getPropertyValue(propertyName);
                styleString += propertyName + ': ' + propertyValue + '; ';
            }

            // Set the computed style string in the style attribute of the element
            rootElement.setAttribute('style', styleString);

            var allElements = rootElement.querySelectorAll('*');

            // Iterate over each element
            allElements.forEach(function(element) {
                // Compute the computed style for the element
                var computedStyle = window.getComputedStyle(element);

                // Create a string representation of the computed styles
                var styleString = '';
                for (var i = 0; i < computedStyle.length; i++) {
                    var propertyName = computedStyle[i];
                    var propertyValue = computedStyle.getPropertyValue(propertyName);
                    styleString += propertyName + ': ' + propertyValue + '; ';
                }

                // Set the computed style string in the style attribute of the element
                element.setAttribute('style', styleString);
            });
            result += rootElement.outerHTML;
            return result;
        }
        return computeAndSetCSS('html') ; 
        """)
        self.code = self.remove_classes(self.code)
        time.sleep(5)
        driver.quit()

    def remove_classes(self,code) : 
        soup = BeautifulSoup(code, 'html.parser')
        elements_with_class = soup.find_all(attrs={'class': True})
        for element in elements_with_class:
            element.attrs.pop('class', None)
        return soup.prettify()

    def without_style(self,code) : 
        soup = BeautifulSoup(code, 'html.parser')
        elements_with_class = soup.find_all(attrs={'class': True})
        for element in elements_with_class:
            element.attrs.pop('style', None)
        return soup.prettify()   

    def get_page_intial_no_css(self,code_no_css) : 
        options = Options()
        print(code_no_css,file = open("tmp.html","w"))
        path = os.path.abspath("tmp.html")
        #options.add_argument("--headless")
        options.add_argument("--no-remote")
        options.add_argument("--new-instance")
        driver = webdriver.Firefox(options=options)
        driver.get("file://" + path)
        time.sleep(5)
        computed_default  = driver.execute_script("""
        function computeAndSetCSS(root) {
            var result = "";
            // Get all elements in the document
            rootElement = document.querySelector(root);
            var computedStyle = window.getComputedStyle(rootElement);

            // Create a string representation of the computed styles
            var styleString = '';
            for (var i = 0; i < computedStyle.length; i++) {
                var propertyName = computedStyle[i];
                var propertyValue = computedStyle.getPropertyValue(propertyName);
                styleString += propertyName + ': ' + propertyValue + '; ';
            }

            // Set the computed style string in the style attribute of the element
            rootElement.setAttribute('style', styleString);

            var allElements = rootElement.querySelectorAll('*');

            // Iterate over each element
            allElements.forEach(function(element) {
                // Compute the computed style for the element
                var computedStyle = window.getComputedStyle(element);

                // Create a string representation of the computed styles
                var styleString = '';
                for (var i = 0; i < computedStyle.length; i++) {
                    var propertyName = computedStyle[i];
                    var propertyValue = computedStyle.getPropertyValue(propertyName);
                    styleString += propertyName + ': ' + propertyValue + '; ';
                }

                // Set the computed style string in the style attribute of the element
                element.setAttribute('style', styleString);
            });
            result += rootElement.outerHTML;
            return result;
        }
        return computeAndSetCSS('html') ; 
        """)
        time.sleep(5)
        driver.quit()
        return computed_default

    def show_result(self,code_no_css) : 
        options = Options()
        print(code_no_css,file = open("tmp.html","w"))
        path = os.path.abspath("tmp.html")
        #options.add_argument("--headless")
        options.add_argument("--no-remote")
        options.add_argument("--new-instance")
        driver = webdriver.Firefox(options=options)
        driver.get("file://" + path)
        time.sleep(5)

s = StealPage()
print(s.code)




